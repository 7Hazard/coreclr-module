#include "stdafx.h"

API * API::instance = nullptr;

extern "C"
{
	EXPORT bool Validate(API * api)
	{
		API::Set(api);
		return true;
	}

	EXPORT void OnModuleInit()
	{
#ifdef _DEBUG
		//Sleep(3000);
#endif // _DEBUG
		CoreCLR::Init();
	}

	EXPORT const char* OnResourceTypeRegister()
	{
		return ".NET Core";
	}

	EXPORT bool OnResourceLoad(const char* resource)
	{
		CoreCLR::Invoke(L"LoadResource", (void**)resource);
		return true;
	}

	EXPORT bool OnTick()
	{
		//
		return true;
	}

	EXPORT bool OnPlayerConnect(long playerid)
	{
		//CoreCLR::Event(L"TriggerOnPlayerConnect", (void**)playerid);
		return true;
	}

	/*EXPORT char* OnHTTPRequest(const char* method, const char* url, const char* query, std::string body)
	{
	return ;
	}*/

	EXPORT bool OnServerCommand(std::string command)
	{
		//
		return true;
	}

	EXPORT bool OnPlayerDisconnect(long playerid, int reason)
	{
		//
		return true;
	}

	EXPORT bool OnPlayerUpdate(long playerid)
	{
		//
		return true;
	}

	EXPORT bool OnKeyStateChanged(long playerid, int keycode, bool isUp)
	{
		//
		return true;
	}

	EXPORT void OnEvent(const char* e, MValueList& args)
	{
		int size = args.size();
		std::vector<void*> values(size);
		for (int i = 0; i < size; i++) {
			std::shared_ptr<MValue> value = args.at(i);
			switch (value->getType())
			{
			case M_STRING:
			{
				std::string& str = value->getString();
				const char* val = str.c_str();
				values[i] = &val;
				break;
			}
			case M_INT: {
				values[i] = (void*)value->getInt();
				break;
			}
			case M_BOOL:
			{
				bool val = value->getBool();
				values[i] = &val;
				break;
			}
			case M_UINT:
			{
				bool val = value->getUInt();
				values[i] = &val;
				break;
			}
			case M_DOUBLE:
			{
				//values.push_back(value->getDouble());
				APIPrint("Warning: Doubles passed in event are not supported yet by CoreCLR Module!");
				values[i] = NULL;
				break;
			}
			case M_NIL:
				values[i] = NULL;
				break;
			case M_DICT:
				/*auto int_keys = value->getIntDict();
				auto string_keys = value->getStringDict();*/
				APIPrint("Warning: Dictionaries in events are not supported yet by CoreCLR Module!");
				values[i] = NULL;
				break;
			}
		}
		if (size > 0) {
			/*void* params[3]{ (void*)e, *values, (void*)size };
			CoreCLR::InvokeArray(L"ServerEvent", params);*/

			CoreCLR::Invoke(L"ServerEvent", (void*)e, &values[0], (void*)size);
			return;
		}
		CoreCLR::Invoke(L"ServerEvent", (void*)e);
	}
}

void APIPrint(std::string msg) {
	std::string out = "[CoreCLR] " + msg;
	API::instance->Print(out.c_str());
}