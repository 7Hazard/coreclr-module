#pragma once
#include "../stdafx.h"
#include "mscoree.h"
#include "coreclrhost.h"

namespace Type {
	typedef void(__stdcall Init)();
	typedef void(__stdcall LoadResource)(const char* resource);
}
namespace Delegate {
	static void *Init, *LoadResource;
}
namespace Method {
	void LoadResource(const char* resource);
}

namespace CoreCLR {
#ifdef _WINDOWS
	static std::wstring CoreVer;
	static std::wstring CorePath;
	static std::wstring CoreDir;

	static HRESULT HResult;
	static ICLRRuntimeHost2* RuntimeHost;
	static DWORD DomainID;

	namespace Params {
		typedef void(__stdcall Single)(void* arg);
		typedef void(__stdcall Double)(void* arg1, void* arg2);
		typedef void(__stdcall Triple)(void* arg1, void* arg2, void* arg3);
		typedef void(__stdcall ParamArray)(void** arg);
	}

	void Invoke(wchar_t* method, void* arg);
	void Invoke(wchar_t* method, void* arg1, void* arg2);
	void Invoke(wchar_t* method, void* arg1, void* arg2, void* arg3);
	void InvokeArray(wchar_t* method, void** arg);
	wchar_t* GetTPAList();
#else
	static const char* CoreVer;
	const char* SetTPAList();
#endif

	void Init();

	/*void LoadResource(std::string resource);
	std::wstring s2ws(const std::string& str);*/
	
	void* GetDelegate(wchar_t* method);
}