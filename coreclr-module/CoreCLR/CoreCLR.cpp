#include "CoreCLR.h"

namespace Method {
	void LoadResource(const char* resource) {
		void *pfnDelegate = NULL;
		CoreCLR::HResult = CoreCLR::RuntimeHost->CreateDelegate(
			CoreCLR::DomainID,
			L"SharpOrange",
			L"SharpOrange.Main",
			L"LoadResource",
			(INT_PTR*)&pfnDelegate
		);
		if (FAILED(CoreCLR::HResult))
		{
			printf("[CoreCLR] ERROR - Failed to create a delegate! (Error code: %x)\n", CoreCLR::HResult);
			return;
		}

		((Type::LoadResource*)pfnDelegate)(resource);
	}
}

namespace CoreCLR {
	void Init() {
#ifdef _WINDOWS
		CoreVer = L"1.1.2";
		CoreDir = L"C:\\Program Files\\dotnet\\shared\\Microsoft.NETCore.App\\" + CoreVer + L"\\";
		CorePath = CoreDir + L"\\coreclr.dll";

		HMODULE CLRHost = LoadLibrary(CorePath.c_str());
		if (!CLRHost) {
			printf("[CoreCLR] ERROR - Failed to load CoreCLR runtime!\n");
			return;
		}

		FnGetCLRRuntimeHost pfnGetCLRRuntimeHost =
			(FnGetCLRRuntimeHost)::GetProcAddress(CLRHost, "GetCLRRuntimeHost");

		if (!pfnGetCLRRuntimeHost)
		{
			printf("[CoreCLR] ERROR - GetCLRRuntimeHost not found\n");
			return;
		}

		HResult = pfnGetCLRRuntimeHost(IID_ICLRRuntimeHost2, (IUnknown**)&RuntimeHost);

		HResult = RuntimeHost->SetStartupFlags(
			static_cast<STARTUP_FLAGS>(
				// STARTUP_FLAGS::STARTUP_SERVER_GC |
				// STARTUP_FLAGS::STARTUP_LOADER_OPTIMIZATION_MULTI_DOMAIN |
				// STARTUP_FLAGS::STARTUP_LOADER_OPTIMIZATION_MULTI_DOMAIN_HOST |
				STARTUP_FLAGS::STARTUP_CONCURRENT_GC |
				STARTUP_FLAGS::STARTUP_SINGLE_APPDOMAIN |
				STARTUP_FLAGS::STARTUP_LOADER_OPTIMIZATION_SINGLE_DOMAIN
				)
		);

		if (FAILED(HResult))
		{
			printf("[CoreCLR] ERROR - Failed to set startup flags! (Error code: %x)\n", HResult);
			return;
		}

		HResult = RuntimeHost->Start();

		int appDomainFlags =
			// APPDOMAIN_FORCE_TRIVIAL_WAIT_OPERATIONS |
			// APPDOMAIN_SECURITY_SANDBOXED |
			APPDOMAIN_ENABLE_PLATFORM_SPECIFIC_APPS |
			APPDOMAIN_ENABLE_PINVOKE_AND_CLASSIC_COMINTEROP |
			APPDOMAIN_DISABLE_TRANSPARENCY_ENFORCEMENT;

		wchar_t* TPAList = GetTPAList();

		wchar_t targetApp[MAX_PATH];
		GetFullPathNameW(L"modules\\coreclr-module", MAX_PATH, targetApp, NULL);
		wchar_t targetAppPath[MAX_PATH];
		wcscpy_s(targetAppPath, targetApp);
		size_t i = wcslen(targetAppPath - 1);
		while (i >= 0 && targetAppPath[i] != L'\\') i--;
		targetAppPath[i] = L'\0';
		wchar_t AppPaths[MAX_PATH * 50];
		wcscpy_s(AppPaths, targetAppPath);

		wchar_t* AppDomainCompatSwitch = L"UseLatestBehaviorWhenTFMNotSpecified";

		const wchar_t* propertyKeys[] = {
			L"TRUSTED_PLATFORM_ASSEMBLIES",
			L"APP_PATHS",
			L"AppDomainCompatSwitch"
		};
		const wchar_t* propertyValues[] = {
			TPAList,
			AppPaths,
			AppDomainCompatSwitch
		};

		HResult = RuntimeHost->CreateAppDomainWithManager(
			L"GTAOrange",
			appDomainFlags,
			NULL,
			NULL,
			sizeof(propertyKeys) / sizeof(wchar_t*),
			propertyKeys,
			propertyValues,
			&DomainID
		);
		if (FAILED(HResult)) {
			printf("[CoreCLR] ERROR - Failed to create AppDomain! (Error code: %x)\n", HResult);
		}

		Invoke(L"Init", NULL);
	}

	void Invoke(wchar_t* method, void* arg) {
		void *pfnDelegate = NULL;
		HResult = RuntimeHost->CreateDelegate(
			DomainID,
			L"SharpOrange",
			L"SharpOrange.SharpOrange",
			method,
			(INT_PTR*)&pfnDelegate
		);
		if (FAILED(HResult))
		{
			printf("[CoreCLR] ERROR - Failed to create a delegate! (Error code: %x)\n", HResult);
			return;
		}

		((Params::Single*)pfnDelegate)(arg);
	}

	void Invoke(wchar_t* method, void* arg1, void* arg2) {
		void *pfnDelegate = NULL;
		HResult = RuntimeHost->CreateDelegate(
			DomainID,
			L"SharpOrange",
			L"SharpOrange.SharpOrange",
			method,
			(INT_PTR*)&pfnDelegate
		);
		if (FAILED(HResult))
		{
			printf("[CoreCLR] ERROR - Failed to create a delegate! (Error code: %x)\n", HResult);
			return;
		}

		((Params::Double*)pfnDelegate)(arg1, arg2);
	}

	void Invoke(wchar_t* method, void* arg1, void* arg2, void* arg3) {
		void *pfnDelegate = NULL;
		HResult = RuntimeHost->CreateDelegate(
			DomainID,
			L"SharpOrange",
			L"SharpOrange.SharpOrange",
			method,
			(INT_PTR*)&pfnDelegate
		);
		if (FAILED(HResult))
		{
			printf("[CoreCLR] ERROR - Failed to create a delegate! (Error code: %x)\n", HResult);
			return;
		}

		((Params::Triple*)pfnDelegate)(arg1, arg2, arg3);
	}

	void InvokeArray(wchar_t* method, void** arg) {
		void *pfnDelegate = NULL;
		HResult = RuntimeHost->CreateDelegate(
			DomainID,
			L"SharpOrange",
			L"SharpOrange.SharpOrange",
			method,
			(INT_PTR*)&pfnDelegate
		);
		if (FAILED(HResult))
		{
			printf("[CoreCLR] ERROR - Failed to create a delegate! (Error code: %x)\n", HResult);
			return;
		}

		((Params::ParamArray*)pfnDelegate)(arg);
	}

	wchar_t* GetTPAList() {
		int tpaSize = 100 * MAX_PATH;
		wchar_t* TPAList = new wchar_t[tpaSize];
		TPAList[0] = L'\0';

		wchar_t *tpaExtensions[] = {
			L"*.dll",
			L"*.exe",
			L"*.winmd"
		};

		for (int i = 0; i < _countof(tpaExtensions); i++)
		{
			wchar_t searchPath[MAX_PATH];
			wcscpy_s(searchPath, MAX_PATH, CoreDir.c_str());
			wcscat_s(searchPath, MAX_PATH, tpaExtensions[i]);

			WIN32_FIND_DATAW findData;
			HANDLE fileHandle = FindFirstFileW(searchPath, &findData);

			if (fileHandle != INVALID_HANDLE_VALUE)
			{
				do
				{
					wchar_t pathToAdd[MAX_PATH];
					wcscpy_s(pathToAdd, MAX_PATH, CoreDir.c_str());
					wcscat_s(pathToAdd, MAX_PATH, findData.cFileName);

					if (wcslen(pathToAdd) + (3) + wcslen(TPAList) >= tpaSize)
					{
						tpaSize *= 2;
						wchar_t* newTPAList = new wchar_t[tpaSize];
						wcscpy_s(newTPAList, tpaSize, TPAList);
						TPAList = newTPAList;
					}

					wcscat_s(TPAList, tpaSize, pathToAdd);
					wcscat_s(TPAList, tpaSize, L";");

				} while (FindNextFileW(fileHandle, &findData));
				FindClose(fileHandle);
			}
		}
		return TPAList;
	}
#else
#ifdef _LINUX
	void* CLRHost = dlopen("libcoreclr.so", RTLD_NOW | RTLD_LOCAL);
#else
	void* CLRHost = dlopen("libcoreclr.dylib", RTLD_NOW | RTLD_LOCAL);
#endif
	wchar_t* GetTPAList()
	{
		const char* const tpaExtensions[] = {
			".ni.dll",      // Probe for .ni.dll first so that it's preferred if ni and il coexist in the same dir
			".dll",
			".ni.exe",
			".exe",
		};

		// TODO fix function
		return TPAList;
	}
#endif
}