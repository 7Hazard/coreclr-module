// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#define WIN32_LEAN_AND_MEAN

#ifdef _WINDOWS
#include <windows.h>
#else
#include <cstring>
#include <string.h>
#endif

#include <cmath>
#include <sstream>
#include <vector>
#include <map>
#include <xmmintrin.h>

#include "API/CVector3.h"
#include "API/API.h"

#include "CoreCLR/CoreCLR.h"
#include "CoreCLR/mscoree.h"

#include "export.h"
#include "coreclr-module.h"