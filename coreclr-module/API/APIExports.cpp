#include "../stdafx.h"

extern "C" {
	/* Server */
	EXPORT void Print(const char* msg) {
		API::instance->Print(msg); 
	}

	/* Player */
	EXPORT void KickPlayer(long playerid, const char* reason) {
		if (reason == nullptr) {
			API::instance->KickPlayer(playerid);
		}
		API::instance->KickPlayer(playerid, reason);
	}
	EXPORT const char* GetPlayerName(long playerid) {
		return API::instance->GetPlayerName(playerid).c_str(); 
	}
	EXPORT void SetPlayerName(long playerid, const char* name) {
		API::instance->SetPlayerName(playerid, name);
	}
	EXPORT float& GetPlayerPosition(int playerid) {
		CVector3 vector = API::instance->GetPlayerPosition(playerid);
		float pos[3] {vector.fX, vector.fY, vector.fZ};
		return *pos;
	}
	EXPORT void SetPlayerPosition(long playerid, float x, float y, float z) {
		API::instance->SetPlayerPosition(playerid, x, y, z);
	}
}