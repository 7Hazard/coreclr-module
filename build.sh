#!/bin/bash

mkdir bin
cd bin
cmake -G "Unix Makefiles" ../coreclr-module/
make VERBOSE=1
msbuild ../SharpOrange/SharpOrange.csproj
cd ../
mkdir linux
cd linux
cp ../bin/coreclr-module.so .
mkdir coreclr-module
cd coreclr-module
cp ../bin/SharpOrange/SharpOrange.dll .
cp ../SharpOrange/Std-symbols.so .