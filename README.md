![](http://i.imgur.com/uVQl3aG.png)
![](http://i.imgur.com/nL74XIX.png)
![](http://i.imgur.com/qF4DQKg.png)

# DEVELOPMENT DISCONTINUED - REFER TO [this C# module for alt:V](https://github.com/FabianTerhorst/coreclr-module)

### CoreCLR module for GTA Orange (https://gta-orange.net)

### [Documentation](https://gta-orange.net/api/CSharp/.NET_Core)

NuGet Package: https://www.nuget.org/packages/SharpOrange/