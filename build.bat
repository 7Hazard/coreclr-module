@echo off

if exist "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VsDevCmd.bat" (
  call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VsDevCmd.bat"
) else (
  if exist "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat" (
    call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat"
  ) else (
    echo Script not found Visual Studio 2017 VsDevCmd.bat
    goto END
  )
)

msbuild coreclr-module\coreclr-module.vcxproj /p:Configuration=Release;Platform=x64 /p:OutputPath=..\bin\
msbuild SharpOrange\SharpOrange.csproj /p:Configuration=Release;Platform="Any CPU" /p:OutputPath=..\bin\
copy bin\coreclr-module.dll windows\coreclr-module.dll
copy bin\SharpOrange.dll windows\coreclr-module\SharpOrange.dll
copy SharpOrange\Std-symbols.dll windows\coreclr-module\Std-symbols.dll

:END