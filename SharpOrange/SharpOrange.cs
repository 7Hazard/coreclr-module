﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Loader;

namespace SharpOrange
{
    static class SharpOrange
    {
        /*internal static bool SMMode = false;
        public static bool SelfManagedMode
        {
            get
            {
                return SMMode;
            }
        }*/

        static void Init()
        {
            Server.Players = new Dictionary<long, Player>();
            APIPrint("Initialized successfully");
        }

        static List<string> LoadedResources = new List<string>();
        static void LoadResource(string resource)
        {
            if (LoadedResources.Contains(resource))
            {
                APIPrint("Attempted to load resource '"+resource+"' again.");
                return;
            }

            Assembly assembly;
            try
            {
                assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(Directory.GetCurrentDirectory()+@"/resources/"+resource+"/"+resource+".dll");
            }
            catch (Exception e)
            {
                APIPrint("Error when loading assembly '"+resource+"'.dll!\n"+e);
                return;
            }
            var entry = assembly.GetType(resource+"."+resource);
            var instance = Activator.CreateInstance(entry);
            LoadedResources.Add(resource);
            APIPrint("Loaded resource '" + resource + "'.");
        }

        static void ServerEvent(string e, 
                [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)]
                IntPtr[] args,
                int size)
            {
                // DEBUG CODE START 
                APIPrint($"Event '{e}' triggered with arguments (size: {size}/{args.Length}):");
                foreach (object arg in args)
                {
                    Console.WriteLine("IntPtr: "+arg);
                }
                object obj = new object();
                try
                {
                    Marshal.PtrToStructure<object>(args[0], obj);
                } catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            // DEBUG CODE END

            /*switch (e)
            {
                case "PlayerConnect":
                    Event.TriggerOnPlayerConnect((long)args[0]);
                    break;
                default:
                    Event.TriggerOnEvent(e, args);
                    break;
            }*/
            }

            internal static void APIPrint(string msg)
        {
            Server.Print("[SharpOrange] " + msg);
        }
    }
}
