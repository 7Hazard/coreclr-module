﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpOrange
{
    public static class Event
    {
        public delegate void OnPlayerConnectHandler(Player player);
        public static event OnPlayerConnectHandler OnPlayerConnect;
        internal static void TriggerOnPlayerConnect(long playerid)
        {
            Player player = new Player(playerid);
            Server.Players.Add(playerid, player);
            OnPlayerConnect(player);
        }

        public delegate void OnEventHandler(string eventname, object[] args);
        public static event OnEventHandler OnEvent;
        internal static void TriggerOnEvent(string eventname, object[] args)
        {
            OnEvent(eventname, args);
        }
    }
}
