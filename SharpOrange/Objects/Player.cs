﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpOrange
{
    public struct Player
    {
        internal Player(long playerid)
        {
            ID = playerid;
            Name = API.GetPlayerName(ID);
            float[] pos = API.GetPlayerPosition(ID);
            Position = new Vector3(pos[0], pos[1], pos[2]);
        }

        public long ID { get; }
        public string Name { get; set; }
        public Vector3 Position { get; set; }
    }
}
