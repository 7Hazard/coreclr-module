﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpOrange
{
    public struct Vector3
    {
        public float x, y, z;
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}
