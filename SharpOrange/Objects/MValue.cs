﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace SharpOrange
{
    [StructLayout(LayoutKind.Explicit)]
    struct MVal
    {
        [FieldOffset(0)] internal bool bool_val;
        [FieldOffset(0)] internal int int_val;
        [FieldOffset(0)] internal uint uint_val;
        [FieldOffset(0)] internal double double_val;
        [FieldOffset(0)] internal string string_val;
        [FieldOffset(0)] internal IntPtr dict_val;
    }

    enum Type
    {
        M_NIL,
        M_BOOL,
        M_INT,
        M_UINT,
        M_DOUBLE,
        M_STRING,
        M_DICT
    }

    struct MDict
    {
        Dictionary<int, IntPtr> int_keys;
        Dictionary<string, IntPtr> string_keys;
    }

    internal class MValue
    {
        Type type;
        MVal val;

        MValue()
        {
            type = Type.M_NIL;
        }

        MValue(bool _val)
        {
            type = Type.M_BOOL;
            val.bool_val = _val;
        }

        MValue(int _val)
        {
            type = Type.M_INT;
            val.int_val = _val;
        }

        MValue(uint _val)
        {
            type = Type.M_UINT;
            val.uint_val = _val;
        }

        MValue(ulong _val)
        {
            type = Type.M_UINT;
            val.uint_val = (uint)_val;
        }

        MValue(double _val)
        {
            type = Type.M_DOUBLE;
            val.double_val = _val;
        }

        MValue(string _val) {
            type = Type.M_STRING;
		    val.string_val = _val;
        }

        /*~MValue()
        {
            switch (type)
            {
                case Type.M_STRING:
                    free(string_val);
                    break;
                case M_DICT:
                    delete dict_val;
                    break;
                default:
                    break;
            }
        }*/
    }
}
