﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace SharpOrange
{
    internal static class API
    {
        [DllImport("coreclr-module", EntryPoint = "GetPlayerName", CallingConvention = CallingConvention.Cdecl)]
        internal static extern string GetPlayerName(long playerid);

        [DllImport("coreclr-module", EntryPoint = "SetPlayerName", CallingConvention = CallingConvention.Cdecl)]
        internal static extern void SetPlayerName(long playerid, string name);

        [DllImport("coreclr-module", EntryPoint = "GetPlayerPosition", CallingConvention = CallingConvention.Cdecl)]
        internal static extern ref float[] GetPlayerPosition(long playerid);

        [DllImport("coreclr-module", EntryPoint = "SetPlayerPosition", CallingConvention = CallingConvention.Cdecl)]
        internal static extern void SetPlayerPosition(long playerid, float x, float y, float z);
    }
}
